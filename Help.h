#ifndef HELP_H_INCLUDED
#define HELP_H_INCLUDED

/*    FICHIER D'AIDE

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[*] LISTE DES NOUVEAUX TYPES :

- Image :
    Contient 3 attributs :
        - surface :
            Une SDL_Surface qui contient les donn�es de l'image
        - rect_src :
            Correspond � la zone de l'image que l'on veut r�cup�rer
        - rect_dst :
            Correspond � la zone o� l'on veut afficher l'image sur l'�cran

    Contient 2 m�thodes :
        - void ImageSetColorKey(Image &image, int r, int g, int b) :
            Permet de transformer tous les pixels de couleur (r,g,b) en pixel transparent.
        - void ImageResize(Image &image_src, Image &image_dst, int w, int h);
            Prend "image_src", la redimensionne � la largeur "w" et � la hauteur "h" et la retourne
            dans "image_dst".

    1 fonction se sert de ce type :
        - void WindowCopy(Window &window, Image &image) :
            Affiche "image" sur la fenetre "window".
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[#] COMMENT CREER UNE NOUVELLE STRUCTURE PROPREMENT ?

Chaque structure que vous cr�ez doit suivre deux r�gles :
- Elle doit avoir une fonction � c�t� qui permet d'initialiser ses attributs
- Elle doit avoir une fonction � c�t� qui permet de d�truire les attributs qui doivent �tre d�truits.

Exemple :

    Voici une structure "Voiture" :

        ###### HEADER ##############################################################################
        struct Voiture
        {
            string marque_de_la_voiture;
            Image image_de_la_voiture;
            int vitesse_de_la_voiture;
            int vitesse_max_de_la_voiture;
        }

        //Je cr�e la fonction qui va servir � initialiser ses attributs :
        Voiture CreateVoiture(string marque, Image image_voiture, int vitesse_max);

        //Je cr�e la fonction qui va servir � d�truire les attributs qui doivent �tre d�truits :
        void DeleteVoiture(Voiture &voiture);
        ############################################################################################


        ###### SOURCE ##############################################################################
        //Voici maintenant le contenu de ces fonctions :

        Voiture CreateVoiture(string marque, Image image_voiture, int vitesse_max)
        {
            Voiture voiture;
            voiture.marque_de_la_voiture       = marque;
            voiture.image_voiture              = image_voiture;
            voiture.vitesse_de_la_voiture      = 0;
            voiture.vitesse_max_de_la_voiture  = vitesse_max;
        }

        void DeleteVoiture(Voiture &voiture)
        {
            DeleteImage(voiture.image_de_la_voiture);
        }
        ############################################################################################

    Cette structure n'a aucune "m�thode". Une m�thode est une action ou une fonction qui se sert de sa structure
    et de ses attributs. Avec l'exemple de la structure Voiture on pourrait rajouter:

        ###### HEADER ##############################################################################
        void VoitureRouler(Voiture &voiture, int vitesse);         //M�thode qui permet de d�finir la vitesse de la voiture
        void VoitureAmeliorer(Voiture &voiture, int vitesse_max);  //M�thode qui permet de red�finir la vitesse maximale de la voiture
        ############################################################################################


        ###### SOURCE ##############################################################################
        void VoitureRouler(Voiture &voiture, int vitesse)
        {
            if (vitesse < voiture.vitesse_max_de_la_voiture)    //On v�rifie que la vitesse donn�e ne d�passe pas la vitesse maximale de la voiture
                voiture.vitesse_de_la_voiture = vitesse;        //Si non alors la voiture prend la vitesse donn�e en param�tre de l'action.
        }

        void VoitureAmeliorer(Voiture &voiture, int vitesse_max)
        {
            voiture.vitesse_max_de_la_voiture = max_vitesse;    //La voiture prend la vitesse maximale donn�e.
        }
        ############################################################################################

Fin de l'exemple

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[*] COMMENT UTILISER UNE STRUCTURE ?

    Reprenons l'exemple de la question pr�c�dente, c'est-�-dire la structure d'une voiture :

        ###### HEADER ##############################################################################
        struct Voiture
        {
            string marque_de_la_voiture;
            Image image_de_la_voiture;
            int vitesse_de_la_voiture;
            int vitesse_max_de_la_voiture;
        }

        //Je cr�e la fonction qui va servir � initialiser ses attributs :
        Voiture CreateVoiture(string marque, Image image_voiture, int vitesse_max);

        //Je cr�e la fonction qui va servir � d�truire les attributs qui doivent �tre d�truits :
        void DeleteVoiture(Voiture &voiture);


        //M�thode qui permet de d�finir la vitesse de la voiture
        void VoitureRouler(Voiture &voiture, int vitesse);

        //M�thode qui permet de red�finir la vitesse maximale de la voiture
        void VoitureAmeliorer(Voiture &voiture, int vitesse_max);
        ############################################################################################

    Voici comment l'utiliser :

        // Tout d'abord on cr�� une variable de cette structure :
        Voiture maVoiture;

        // Ensuite on l'initialise avec la fonction "CreateVoiture" :
        maVoiture = CreateVoiture("cabriolet", image_voiture, 250);

        // On peut utiliser ses m�thodes :
        VoitureRouler(maVoiture, 120);     //maVoiture roule maintenant � 120 km/h
        VoitureAmeliorer(maVoiture, 280);  //maVoiture peut maintenant aller jusqu'� 280 km/h

        // Apr�s s'en �tre servi on la d�truit :
        DeleteVoiture(maVoiture);

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[#] COMMENT SAVOIR QUELLES ATTRIBUTS DOIVENT ETRE DETRUITS ?

D'une tr�s simple mani�re, voici la liste des types qui n�cessitent que leurs instances (variables) soient d�truites :

//MERCI DE COMPLETER LA LISTE SI VOUS EN RAJOUTEZ

- Image   : DeleteImage(Image)
- Window  : DeleteWindow(Window)
- Game    : DeleteGame(Game)
- Menu    : DeleteMenu(Menu)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A COMPLETER
*/

#endif // HELP_H_INCLUDED
