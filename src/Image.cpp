#include "Image.h"

Image CreateImage(std::string path)
{
    Image image;
    image.surface = IMG_Load(path.c_str());
    if (image.surface != 0)
    {
        image.rect_src = image.surface->clip_rect;
        image.rect_dst = image.surface->clip_rect;
    }
    return image;
}

void DeleteImage(Image &image)
{
    if (image.surface != 0)
        SDL_FreeSurface(image.surface);
}

void ImageSetColorKey(Image &image, int r, int g, int b)
{
    if (image.surface != 0)
    {
        SDL_Surface* optimized_image = SDL_DisplayFormat(image.surface);
        if(optimized_image != 0)
        {
            SDL_FreeSurface(image.surface);
            image.surface = optimized_image;
            Uint32 colorkey = SDL_MapRGB(image.surface->format, r, g, b);
            SDL_SetColorKey(image.surface, SDL_SRCCOLORKEY, colorkey);
        }
    }
}

void ImageResizeSize(Image &image_src, Image &image_dst, int w, int h)
{
    if (image_src.surface != 0)
    {
        float zoomX = (float) w / image_src.rect_dst.w;
        float zoomY = (float) h / image_src.rect_dst.h;

        if (image_dst.surface != 0)
            DeleteImage(image_dst);

        image_dst.surface = rotozoomSurfaceXY(image_src.surface, 0, zoomX, zoomY, 0);
    }
}

void ImageResize(Image &image_src, Image &image_dst, float zoomX, float zoomY)
{
    if (image_src.surface != 0)
    {
        if (image_dst.surface != 0)
            DeleteImage(image_dst);

        image_dst.surface = rotozoomSurfaceXY(image_src.surface, 0, zoomX, zoomY, 0);
    }
}

void ImageRotate(Image &image_src, Image &image_dst, float angle)
{
    if (image_src.surface != 0)
        image_dst.surface = rotozoomSurfaceXY(image_src.surface, angle, 1, 1, 0);
}

void ImageResizeAndRotate(Image &image_src, Image &image_dst, int w, int h, float angle)
{
    float zoomX = (float) w / image_src.rect_dst.w;
    float zoomY = (float) h / image_src.rect_dst.h;

    if (image_src.surface != 0)
        image_dst.surface = rotozoomSurfaceXY(image_src.surface, angle, zoomX, zoomY, 0);
}

void ImageOptimize(Image &image)
{
    if (image.surface != 0)
    {
        SDL_Surface* optimized_image = SDL_DisplayFormat(image.surface);
        if (optimized_image != 0)
        {
            SDL_FreeSurface(image.surface);
            image.surface = optimized_image;
        }
    }
}
