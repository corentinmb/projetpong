#include "Properties.h"

Properties CreateProperties(int width, int height)
{
    Properties properties;

    properties.window_width = width;
    properties.window_height = height;

    properties.zoomX = (float) width / WINDOW_WIDTH_BASE;
    properties.zoomY = (float) height / WINDOW_HEIGHT_BASE;

    return properties;
}

void PropertiesReset(Properties &properties, int width, int height)
{
    properties.window_width = width;
    properties.window_height = height;

    properties.zoomX = (float) width / WINDOW_WIDTH_BASE;
    properties.zoomY = (float) height / WINDOW_HEIGHT_BASE;
}
