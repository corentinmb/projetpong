#include "Score.h"

using namespace std;

Score CreateScore()
{
    Score score;

    score.image_base.surface = 0;
    score.image.surface = 0;

    score.image_base = CreateText("0", "ressources/font/cell.ttf", 36, 255, 255, 255);
    score.image = CreateText("0", "ressources/font/cell.ttf", 36, 255, 255, 255);
    score.score = 0;

    return score;
}

void DeleteScore(Score &score)
{
    DeleteImage(score.image_base);
    DeleteImage(score.image);
}

void ScoreResizePlayer1(Score &score, Properties &properties)
{
    ImageResize(score.image_base, score.image, properties.zoomX, properties.zoomY);

    score.image.rect_src = score.image.surface->clip_rect;

    score.image.rect_dst.w = SCORE_SIZE * properties.zoomX;
    score.image.rect_dst.h = SCORE_SIZE * properties.zoomY;
    score.image.rect_dst.x = SCORE_POS_X1 * properties.zoomX;
    score.image.rect_dst.y = SCORE_POS_Y * properties.zoomY;
}

void ScoreResizePlayer2(Score &score, Properties &properties)
{
    ImageResize(score.image_base, score.image, properties.zoomX, properties.zoomY);

    score.image.rect_src = score.image.surface->clip_rect;

    score.image.rect_dst.w = SCORE_SIZE * properties.zoomX;
    score.image.rect_dst.h = SCORE_SIZE * properties.zoomY;
    score.image.rect_dst.x = SCORE_POS_X2 * properties.zoomX;
    score.image.rect_dst.y = SCORE_POS_Y * properties.zoomY;
}

void ScoreSetPos(Score &score, int x, int y)
{
    score.image.rect_dst.x = x;
    score.image.rect_dst.y = y;
}

void ScoreIncrementPlayer1(Score &score, Properties &properties)
{
    score.score++;

    ostringstream mssg;
    mssg.flush();
    mssg.str("");
    mssg << score.score;

    DeleteImage(score.image_base);
    DeleteImage(score.image);
    score.image_base = CreateText(mssg.str(), "ressources/font/cell.ttf", 36, 255, 255, 255);
    score.image = CreateText(mssg.str(), "ressources/font/cell.ttf", 36, 255, 255, 255);
    ScoreResizePlayer1(score, properties);
}

void ScoreIncrementPlayer2(Score &score, Properties &properties)
{
    score.score++;

    ostringstream mssg;
    mssg.flush();
    mssg.str("");
    mssg << score.score;

    DeleteImage(score.image_base);
    DeleteImage(score.image);
    score.image_base = CreateText(mssg.str(), "ressources/font/cell.ttf", 36, 255, 255, 255);
    score.image = CreateText(mssg.str(), "ressources/font/cell.ttf", 36, 255, 255, 255);
    ScoreResizePlayer2(score, properties);
}

void ScoreResetPlayer1(Score &score, Properties &properties)
{
    score.score = 0;
    score.image_base = CreateText("0", "ressources/font/cell.ttf", 36, 255, 255, 255);
    score.image = CreateText("0", "ressources/font/cell.ttf", 36, 255, 255, 255);
    ScoreResizePlayer1(score, properties);
}

void ScoreResetPlayer2(Score &score, Properties &properties)
{
    score.score = 0;
    score.image_base = CreateText("0", "ressources/font/cell.ttf", 36, 255, 255, 255);
    score.image = CreateText("0", "ressources/font/cell.ttf", 36, 255, 255, 255);
    ScoreResizePlayer2(score, properties);
}
