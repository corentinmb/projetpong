#include "Ball.h"

using namespace std;

Ball CreateBall()
{
    Ball ball;
    ball.mode = BALL_CENTERED;
    ball.x = (WINDOW_WIDTH_BASE/2)-(BALL_SIZE/2);
    ball.y = (WINDOW_HEIGHT_BASE/2)-(BALL_SIZE/2);
    ball.angle = GetRandomAngle(0, M_PI / 3, 2 * M_PI / 3, M_PI);

    ball.speed_base = BALL_SPEED_BASE;
    ball.speed = BALL_SPEED_BASE;

    ball.image_base = CreateImage("ressources/img/Ball.png");
    ball.image.surface = 0;

    ImageResizeSize(ball.image_base, ball.image, BALL_SIZE, BALL_SIZE);

    ball.image.rect_dst.x = ball.x;
    ball.image.rect_dst.y = ball.y;
    return ball;
}

void DeleteBall(Ball &ball)
{
    DeleteImage(ball.image);
}

void BallSetPos(Ball &ball, int x, int y, Properties &properties)
{
    ball.x = x;
    ball.y = y;
    ball.image.rect_dst.x = x * properties.zoomX;
    ball.image.rect_dst.y = y * properties.zoomY;
}

void BallMovement(Ball &ball, Properties &properties)
{
    ball.x += cos(ball.angle) * ball.speed;
    ball.y += sin(ball.angle) * ball.speed;

    ball.image.rect_dst.x = ball.x * properties.zoomX;
    ball.image.rect_dst.y = ball.y * properties.zoomY;
}

void BallHeightBorder(Ball &ball, Properties &properties)
{
    if (ball.y + BALL_SIZE > BOUND_Y_MAX)
    {
        ball.y = BOUND_Y_MAX - BALL_SIZE;
        ball.angle = - ball.angle;
    }

    if(ball.y < BOUND_Y_MIN)
    {
        ball.y = BOUND_Y_MIN;
        ball.angle = - ball.angle;
    }
}

void BallCollisionRacket(Ball &ball, Racket &racket_player1, Racket &racket_player2, Properties &properties)
{
    if (ball.x < racket_player1.x + RACKET_SIZE_W)
    {
        float next_pos_y = ball.y + ball.speed * sin(ball.angle);
        if ((next_pos_y + BALL_SIZE > racket_player1.y && next_pos_y < racket_player1.y + RACKET_SIZE_H) &&
            (ball.y + BALL_SIZE > racket_player1.y && ball.y < racket_player1.y + RACKET_SIZE_H))
        {
            ball.angle = M_PI - ball.angle;
            if (racket_player1.mode == RACKET_UP)
                ball.angle -= RACKET_POWER;
            else if (racket_player1.mode == RACKET_DOWN)
                ball.angle += RACKET_POWER;
            if (racket_player1.push == RACKET_PUSHING)
                ball.speed+= 2;
            ball.x = racket_player1.x + RACKET_SIZE_W;
            ball.image.rect_dst.x = ball.x * properties.zoomX;
        }
    }

    if (ball.x + BALL_SIZE > racket_player2.x)
    {
        float next_pos_y = ball.y + ball.speed * sin(ball.angle);
        if ((next_pos_y + BALL_SIZE > racket_player2.y && next_pos_y < racket_player2.y + RACKET_SIZE_H) &&
            (ball.y + BALL_SIZE > racket_player2.y && ball.y < racket_player2.y + RACKET_SIZE_H))
        {
            ball.angle = M_PI - ball.angle;
            if (racket_player2.mode == RACKET_UP)
                ball.angle += RACKET_POWER;
            else if (racket_player2.mode == RACKET_DOWN)
                ball.angle -= RACKET_POWER;
            if (racket_player2.push == RACKET_PUSHING)
                ball.speed+= 2;
            ball.x = racket_player2.x - BALL_SIZE;
            ball.image.rect_dst.x = ball.x * properties.zoomX;
        }
    }
}

void BallFocusOnPlayer1(Ball &ball, Racket &racket_player1, Properties &properties)
{
    ball.x = racket_player1.x + RACKET_SIZE_W;
    ball.y = racket_player1.y + RACKET_SIZE_H / 2 - BALL_SIZE / 2;
    ball.image.rect_dst.x = ball.x * properties.zoomX;
    ball.image.rect_dst.y = ball.y * properties.zoomY;
}

void BallFocusOnPlayer2(Ball &ball, Racket &racket_player2, Properties &properties)
{
    ball.x = racket_player2.x - BALL_SIZE;
    ball.y = racket_player2.y + RACKET_SIZE_H / 2 - BALL_SIZE / 2;
    ball.image.rect_dst.x = ball.x * properties.zoomX;
    ball.image.rect_dst.y = ball.y * properties.zoomY;
}

void BallInitPlayer1 (Ball &ball, Racket &racket_player1, Arrow &arrow_player1, Properties &properties)
{
    RacketReset(racket_player1, properties);
    BallFocusOnPlayer1(ball, racket_player1, properties);
    ArrowFocusOnPlayer1(arrow_player1, racket_player1, properties);
    ball.mode = BALL_FOCUS_RACKET1;
}

void BallInitPlayer2 (Ball &ball, Racket &racket_player2, Arrow &arrow_player2, Properties &properties)
{
    RacketReset(racket_player2, properties);
    BallFocusOnPlayer2(ball, racket_player2, properties);
    ArrowFocusOnPlayer2(arrow_player2, racket_player2, properties);
    ball.mode = BALL_FOCUS_RACKET2;
}

Winner BallScore(Ball &ball, Racket &racket_player1, Racket &racket_player2, Arrow &arrow_player1, Arrow &arrow_player2, Score &score_player1, Score &score_player2, Properties &properties)
{
    if (score_player1.score != SCORE_MAX && score_player2.score != SCORE_MAX)
    {
        if (ball.x + (BALL_SIZE/2) > BOUND_X_MAX && ball.mode == BALL_FREE)
        {
            ScoreIncrementPlayer1(score_player1, properties);
            BallInitPlayer2(ball, racket_player2, arrow_player2, properties);
            ball.speed = BALL_SPEED_BASE;
        }
        else if (ball.x - (BALL_SIZE/2) < BOUND_X_MIN && ball.mode == BALL_FREE)
        {
            ScoreIncrementPlayer2(score_player2, properties);
            BallInitPlayer1(ball, racket_player1, arrow_player1, properties);
            ball.speed = BALL_SPEED_BASE;
        }
        return NONE;
    }
    else if (score_player1.score == SCORE_MAX)
        return PLAYER1;
    else
        return PLAYER2;
}

void BallReset(Ball &ball, Properties &properties)
{
    ball.mode = BALL_CENTERED;
    ball.x = (WINDOW_WIDTH_BASE/2)-(BALL_SIZE/2);
    ball.y = (WINDOW_HEIGHT_BASE/2)-(BALL_SIZE/2);
    ball.image.rect_dst.x = ball.x * properties.zoomX;
    ball.image.rect_dst.y = ball.y * properties.zoomY;
    ball.angle = GetRandomAngle(0, M_PI / 3, 2 * M_PI / 3, M_PI);
    ball.speed = BALL_SPEED_BASE;
}

void BallResize(Ball &ball, Properties &properties)
{
    ImageResizeSize(ball.image_base, ball.image, BALL_SIZE * properties.zoomX, BALL_SIZE * properties.zoomY);

    ball.image.rect_src = ball.image.surface->clip_rect;

    ball.image.rect_dst.x = ball.x * properties.zoomX;
    ball.image.rect_dst.y = ball.y * properties.zoomY;
    ball.image.rect_dst.w = BALL_SIZE * properties.zoomX;
    ball.image.rect_dst.h = BALL_SIZE * properties.zoomY;
}

void BallTakeArrowAngle(Ball &ball, Arrow &arrow)
{
    ball.angle = arrow.angle * (M_PI / 180);
}

float GetRandomAngle(float bound1, float bound2)
{
    float random_angle;
    random_angle = ((rand() % 6280) / 1000.0) - 3.14;

    while (!(random_angle > bound1 && random_angle < bound2) && !(random_angle < -bound1 && random_angle > -bound2))
    {
        random_angle = ((rand() % 6280) / 1000.0) - 3.14;
    }

    return random_angle;
}

float GetRandomAngle(float bound1, float bound2, float bound3, float bound4)
{
    float random_angle1 = GetRandomAngle(bound1, bound2);
    float random_angle2 = GetRandomAngle(bound3, bound4);

    int alea = rand() % 2;

    if (alea)
        return random_angle1;
    else
        return random_angle2;
}
