#include "Window.h"
#include "Menu.h"

/*  ####################   INITIALISATION DU MENU   ##################### */
Menu CreateMenu(Properties properties)
{
    Menu menu;

    menu.posX_title = (WINDOW_WIDTH_BASE - (TITLE_FILE_WIDTH / 2)) / 2;
    menu.posY_title = (WINDOW_HEIGHT_BASE - (TITLE_FILE_HEIGHT / 2)) * 4 / 15;

    menu.posX_button = (WINDOW_WIDTH_BASE - BUTTON_WIDTH) / 2;
    menu.posY_play = (WINDOW_HEIGHT_BASE - BUTTON_HEIGHT) * 11 / 15;
    menu.posY_quit = (WINDOW_HEIGHT_BASE - BUTTON_HEIGHT) * 14 / 15;

    menu.back_base = CreateImage("ressources/img/Menu_fond.png");
    ImageOptimize(menu.back_base);
    menu.title_base = CreateImage("ressources/img/Menu_titre.png");
    menu.button_base = CreateImage("ressources/img/Button.png");
    ImageOptimize(menu.button_base);

    menu.back.surface = 0;
    menu.title.surface = 0;
    menu.button.surface = 0;

    ImageResizeSize(menu.back_base, menu.back, WINDOW_WIDTH_BASE, WINDOW_HEIGHT_BASE);
    ImageResizeSize(menu.title_base, menu.title, TITLE_FILE_WIDTH / 2, TITLE_FILE_HEIGHT / 2);
    ImageSetColorKey(menu.title, 0, 0, 0);
    ImageResizeSize(menu.button_base, menu.button, BUTTON_FILE_WIDTH, BUTTON_FILE_HEIGHT);
    ImageSetColorKey(menu.button, 0, 0, 0);

    menu.title.rect_dst.x = menu.posX_title;
    menu.title.rect_dst.y = menu.posY_title;

    MenuInitRect(menu.rect_src[0], 0, BUTTON_SRC_Y, BUTTON_WIDTH, BUTTON_HEIGHT);
    MenuInitRect(menu.rect_src[1], BUTTON_SRC_X, BUTTON_SRC_Y, BUTTON_WIDTH, BUTTON_HEIGHT);
    MenuInitRect(menu.rect_src[2], 0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
    MenuInitRect(menu.rect_src[3], BUTTON_SRC_X, 0, BUTTON_WIDTH, BUTTON_HEIGHT);

    MenuInitRect(menu.rect_dst[0], menu.posX_button, menu.posY_play, BUTTON_WIDTH, BUTTON_HEIGHT);
    MenuInitRect(menu.rect_dst[1], menu.posX_button, menu.posY_play, BUTTON_WIDTH, BUTTON_HEIGHT);
    MenuInitRect(menu.rect_dst[2], menu.posX_button, menu.posY_quit, BUTTON_WIDTH, BUTTON_HEIGHT);
    MenuInitRect(menu.rect_dst[3], menu.posX_button, menu.posY_quit, BUTTON_WIDTH, BUTTON_HEIGHT);

    return menu;
}
/*  ##################################################################### */


/*  ##################   BOUCLE D'AFFICHAGE DU MENU   ################### */
WindowMode WindowRunMenu(Window &window, Menu &menu)
{
    if (window.screen != 0 && menu.back.surface != 0 && menu.button.surface != 0)
    {
        WindowCopy(window, menu.back);
        WindowCopy(window, menu.title);

        if ((window.input.x > menu.posX_button && window.input.x < menu.posX_button + menu.rect_dst[0].w) &&
            (window.input.y > menu.posY_play && window.input.y < menu.posY_play + menu.rect_dst[0].h))
        {
            SDL_BlitSurface(menu.button.surface, &menu.rect_src[1], window.screen, &menu.rect_dst[1]);
            if (window.input.buttons[SDL_BUTTON_LEFT])
                return GAME;
        }
        else
            SDL_BlitSurface(menu.button.surface, &menu.rect_src[0], window.screen, &menu.rect_dst[0]);

        if ((window.input.x > menu.posX_button && window.input.x < menu.posX_button + menu.rect_dst[2].w) &&
            (window.input.y > menu.posY_quit && window.input.y < menu.posY_quit + menu.rect_dst[2].h))
        {
            SDL_BlitSurface(menu.button.surface, &menu.rect_src[3], window.screen, &menu.rect_dst[3]);
            if (window.input.buttons[SDL_BUTTON_LEFT])
                return END;
        }
        else
            SDL_BlitSurface(menu.button.surface, &menu.rect_src[2], window.screen, &menu.rect_dst[2]);
    }

    return MENU;
}
/*  ##################################################################### */


/*  #################   REDIMENSIONNEMENT DES IMAGES   ################## */
void MenuResize(Menu &menu, Properties properties)
{
    float zoomY = (float) properties.window_height / WINDOW_HEIGHT_BASE;
    float zoomX_elements = (float) zoomY * (WINDOW_WIDTH_BASE / WINDOW_HEIGHT_BASE);
    float zoomX_window = (float) properties.window_width / WINDOW_WIDTH_BASE;

    ImageResizeSize(menu.back_base, menu.back, properties.window_width, properties.window_height);
    menu.back.rect_dst = menu.back.surface->clip_rect;
    menu.back.rect_src = menu.back.surface->clip_rect;

    ImageResizeSize(menu.title_base, menu.title, (TITLE_FILE_WIDTH / 2) * zoomX_elements, (TITLE_FILE_HEIGHT / 2) * zoomY);
    menu.title.rect_dst = menu.title.surface->clip_rect;
    menu.title.rect_src = menu.title.surface->clip_rect;

    ImageResizeSize(menu.button_base, menu.button, BUTTON_FILE_WIDTH * zoomX_elements, BUTTON_FILE_HEIGHT * zoomY);
    ImageSetColorKey(menu.button, 0, 0, 0);

    menu.posY_play = ((WINDOW_HEIGHT_BASE - BUTTON_HEIGHT) * 11 / 15) * zoomY;
    menu.posY_quit = ((WINDOW_HEIGHT_BASE - BUTTON_HEIGHT) * 14 / 15) * zoomY;
    menu.posX_button = (WINDOW_WIDTH_BASE * zoomX_window - BUTTON_WIDTH * zoomX_elements) / 2;

    menu.posX_title = (WINDOW_WIDTH_BASE * zoomX_window - (TITLE_FILE_WIDTH / 2) * zoomX_elements) / 2;
    menu.posY_title = ((WINDOW_HEIGHT_BASE - (TITLE_FILE_HEIGHT / 2)) * 4 / 15) * zoomY;

    menu.title.rect_dst.x = menu.posX_title;
    menu.title.rect_dst.y = menu.posY_title;

    MenuInitRect(menu.rect_src[0], 0, zoomY * BUTTON_SRC_Y, zoomX_elements * BUTTON_WIDTH, zoomY * BUTTON_HEIGHT);
    MenuInitRect(menu.rect_src[1], zoomX_elements * BUTTON_SRC_X, zoomY * BUTTON_SRC_Y, zoomX_elements * BUTTON_WIDTH, zoomY * BUTTON_HEIGHT);
    MenuInitRect(menu.rect_src[2], 0, 0, zoomX_elements * BUTTON_WIDTH, zoomY * BUTTON_HEIGHT);
    MenuInitRect(menu.rect_src[3], zoomX_elements * BUTTON_SRC_X, 0, zoomX_elements * BUTTON_WIDTH, zoomY * BUTTON_HEIGHT);
    MenuInitRect(menu.rect_dst[0], menu.posX_button, menu.posY_play, zoomX_elements * BUTTON_WIDTH, zoomY * BUTTON_HEIGHT);
    MenuInitRect(menu.rect_dst[1], menu.posX_button, menu.posY_play, zoomX_elements * BUTTON_WIDTH, zoomY * BUTTON_HEIGHT);
    MenuInitRect(menu.rect_dst[2], menu.posX_button, menu.posY_quit, zoomX_elements * BUTTON_WIDTH, zoomY * BUTTON_HEIGHT);
    MenuInitRect(menu.rect_dst[3], menu.posX_button, menu.posY_quit, zoomX_elements * BUTTON_WIDTH, zoomY * BUTTON_HEIGHT);
}
/*  ##################################################################### */
