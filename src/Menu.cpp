#include "Menu.h"

void DeleteMenu(Menu &menu)
{
    DeleteImage(menu.back_base);
    DeleteImage(menu.title_base);
    DeleteImage(menu.button_base);
    DeleteImage(menu.back);
    DeleteImage(menu.title);
    DeleteImage(menu.button);
}

void MenuInitRect(SDL_Rect &rect, int x, int y, int w, int h)
{
    rect.x = x;
    rect.y = y;
    rect.w = w;
    rect.h = h;
}

// LE FONCTIONNEMENT DE MENU SE TROUVE DANS LE FICHIER "RunMenu.cpp".
