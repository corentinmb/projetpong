#include "MessageWinner.h"

MessageWinner CreateMessageWinner(PlayerWinner winner)
{
    MessageWinner message;

    if (winner == WINNER_PLAYER1)
        message.message_base = CreateImage("ressources/img/MessageWinner1.png");
    else
        message.message_base = CreateImage("ressources/img/MessageWinner2.png");

    message.message.surface = 0;
    ImageResizeSize(message.message_base, message.message, MESSAGE_WINNER_SIZE_X, MESSAGE_WINNER_SIZE_Y);

    message.button_base = CreateImage("ressources/img/BackToMenuButton.png");
    message.button.surface = 0;
    ImageResizeSize(message.button_base, message.button, BUTTON_BACK_SIZE_X, BUTTON_BACK_SIZE_Y);

    message.button_selected_base = CreateImage("ressources/img/BackToMenuButtonSelected.png");
    message.button_selected.surface = 0;
    ImageResizeSize(message.button_selected_base, message.button_selected, BUTTON_BACK_SIZE_X, BUTTON_BACK_SIZE_Y);

    ImageSetColorKey(message.message_base, 0, 0, 0);
    ImageSetColorKey(message.message, 0, 0, 0);
    ImageOptimize(message.button_base);
    ImageOptimize(message.button);
    ImageOptimize(message.button_selected_base);
    ImageOptimize(message.button_selected);

    message.message.rect_dst.x = MESSAGE_WINNER_POS_X;
    message.message_base.rect_dst.y = MESSAGE_WINNER_POS_Y;

    message.button.rect_dst.x = BUTTON_BACK_POS_X;
    message.button.rect_dst.y = BUTTON_BACK_POS_Y;
    message.button_selected.rect_dst.x = BUTTON_BACK_POS_X;
    message.button_selected.rect_dst.y = BUTTON_BACK_POS_Y;

    return message;
}

void DeleteMessageWinner(MessageWinner &message)
{
    DeleteImage(message.message_base);
    DeleteImage(message.message);
    DeleteImage(message.button_base);
    DeleteImage(message.button);
    DeleteImage(message.button_selected_base);
    DeleteImage(message.button_selected);
}

bool MessageWinnerRun(MessageWinner &message, Properties &properties, Input& input, SDL_Surface* screen)
{
    if ((input.x > message.button.rect_dst.x && input.x < message.button.rect_dst.x + message.button.rect_dst.w) &&
        (input.y > message.button.rect_dst.y && input.y < message.button.rect_dst.y + message.button.rect_dst.h))
    {
        SDL_BlitSurface(message.button_selected.surface, &message.button_selected.rect_src, screen, &message.button_selected.rect_dst);
        if (input.buttons[SDL_BUTTON_LEFT])
            return true;
    }
    else
        SDL_BlitSurface(message.button.surface, &message.button.rect_src, screen, &message.button.rect_dst);
    return false;
}

void MessageWinnerResize(MessageWinner &message, Properties &properties)
{
    ImageResizeSize(message.message_base, message.message, MESSAGE_WINNER_SIZE_X * properties.zoomX, MESSAGE_WINNER_SIZE_Y * properties.zoomY);
    ImageResizeSize(message.button_base, message.button, BUTTON_BACK_SIZE_X * properties.zoomX, BUTTON_BACK_SIZE_Y * properties.zoomY);
    ImageResizeSize(message.button_selected_base, message.button_selected, BUTTON_BACK_SIZE_X * properties.zoomX, BUTTON_BACK_SIZE_Y * properties.zoomY);

    message.message.rect_src = message.message.surface->clip_rect;
    message.message.rect_dst.x = MESSAGE_WINNER_POS_X * properties.zoomX;
    message.message.rect_dst.y = MESSAGE_WINNER_POS_Y * properties.zoomY;
    message.message.rect_dst.w = MESSAGE_WINNER_SIZE_X * properties.zoomX;
    message.message.rect_dst.h = MESSAGE_WINNER_SIZE_Y * properties.zoomY;

    message.button.rect_src = message.message.surface->clip_rect;
    message.button.rect_dst.x = BUTTON_BACK_POS_X * properties.zoomX;
    message.button.rect_dst.y = BUTTON_BACK_POS_Y * properties.zoomY;
    message.button.rect_dst.w = BUTTON_BACK_SIZE_X * properties.zoomX;
    message.button.rect_dst.h = BUTTON_BACK_SIZE_Y * properties.zoomY;

    message.button_selected.rect_src = message.message.surface->clip_rect;
    message.button_selected.rect_dst.x = BUTTON_BACK_POS_X * properties.zoomX;
    message.button_selected.rect_dst.y = BUTTON_BACK_POS_Y * properties.zoomY;
    message.button_selected.rect_dst.w = BUTTON_BACK_SIZE_X * properties.zoomX;
    message.button_selected.rect_dst.h = BUTTON_BACK_SIZE_Y * properties.zoomY;
}
