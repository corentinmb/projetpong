#include "Arrow.h"

Arrow CreateArrow(int x, int y, float angle, Properties &properties)
{
    Arrow arrow;

    arrow.image_base = CreateImage("ressources/img/Arrow.png");
    arrow.image.surface = 0;
    ImageResizeAndRotate(arrow.image_base, arrow.image, ARROW_SIZE_W * properties.zoomX, ARROW_SIZE_H * properties.zoomY, angle);

    arrow.x_base = x;
    arrow.x = x;
    arrow.y_base = y;
    arrow.y = y;
    arrow.angle_base = angle;
    arrow.angle = angle;

    arrow.image.rect_src = arrow.image.surface->clip_rect;

    arrow.image.rect_dst.x = arrow.x * properties.zoomX;
    arrow.image.rect_dst.y = arrow.y * properties.zoomY;
    arrow.image.rect_dst.w = arrow.image.surface->clip_rect.w;
    arrow.image.rect_dst.h = arrow.image.surface->clip_rect.h;

    arrow.mode = ARROW_FREE;

    return arrow;
}

Arrow CreateArrowForPlayer1(Racket &racket, Properties &properties)
{
    Arrow arrow = CreateArrow(racket.x + RACKET_SIZE_W + BALL_SIZE, racket.y + RACKET_SIZE_H / 2 - ARROW_SIZE_H / 2, 0, properties);
    return arrow;
}

Arrow CreateArrowForPlayer2(Racket &racket, Properties &properties)
{
    Arrow arrow = CreateArrow(racket.x - BALL_SIZE - ARROW_SIZE_W, racket.y + RACKET_SIZE_H / 2 - ARROW_SIZE_H / 2, 180, properties);
    return arrow;
}

void DeleteArrow(Arrow &arrow)
{
    DeleteImage(arrow.image);
    DeleteImage(arrow.image_base);
}

void ArrowResize(Arrow &arrow, Properties &properties)
{
    ImageResizeAndRotate(arrow.image_base, arrow.image, ARROW_SIZE_W * properties.zoomX, ARROW_SIZE_H * properties.zoomY, -arrow.angle);

    arrow.image.rect_src = arrow.image.surface->clip_rect;

    arrow.image.rect_dst.x = arrow.x * properties.zoomX;
    arrow.image.rect_dst.y = arrow.y * properties.zoomY;
    arrow.image.rect_dst.w = arrow.image.surface->clip_rect.w;
    arrow.image.rect_dst.h = arrow.image.surface->clip_rect.h;
}

void ArrowReset(Arrow &arrow, Properties &properties)
{
    arrow.x = arrow.x_base;
    arrow.y = arrow.y_base;
    arrow.angle = arrow.angle_base;

    ImageResizeAndRotate(arrow.image_base, arrow.image, ARROW_SIZE_W * properties.zoomX, ARROW_SIZE_H * properties.zoomY, arrow.angle);

    arrow.image.rect_src = arrow.image.surface->clip_rect;

    arrow.image.rect_dst.x = arrow.x * properties.zoomX;
    arrow.image.rect_dst.y = arrow.y * properties.zoomY;
    arrow.image.rect_dst.w = arrow.image.surface->clip_rect.w;
    arrow.image.rect_dst.h = arrow.image.surface->clip_rect.h;
}

void ArrowFocusOnPlayer1(Arrow &arrow, Racket &racket_player1, Properties &properties)
{
    arrow.x = racket_player1.x + RACKET_SIZE_W + BALL_SIZE + 8;
    arrow.y = racket_player1.y + RACKET_SIZE_H / 2 - ARROW_SIZE_H / 2;
    arrow.image.rect_dst.x = arrow.x * properties.zoomX;
    arrow.image.rect_dst.y = arrow.y * properties.zoomY;
}

void ArrowFocusOnPlayer2(Arrow &arrow, Racket &racket_player2, Properties &properties)
{
    arrow.x = racket_player2.x - BALL_SIZE - 8 - ARROW_SIZE_W;
    arrow.y = racket_player2.y + RACKET_SIZE_H / 2 - ARROW_SIZE_H / 2;
    arrow.image.rect_dst.x = arrow.x * properties.zoomX;
    arrow.image.rect_dst.y = arrow.y * properties.zoomY;
}

void ArrowFollowPlayerUp(Arrow &arrow, Racket &racket_player, Properties &properties)
{
    if (racket_player.y > BOUND_Y_MIN)
    {
        arrow.y -= RACKET_SPEED_BASE;
        arrow.image.rect_dst.x = arrow.x * properties.zoomX;
        arrow.image.rect_dst.y = arrow.y * properties.zoomY;
    }
}

void ArrowFollowPlayerDown(Arrow &arrow, Racket &racket_player, Properties &properties)
{
    if (racket_player.y + RACKET_SIZE_H < BOUND_Y_MAX)
    {
        arrow.y += RACKET_SPEED_BASE;
        arrow.image.rect_dst.x = arrow.x * properties.zoomX;
        arrow.image.rect_dst.y = arrow.y * properties.zoomY;
    }
}

void ArrowIncreaseAngle(Arrow &arrow, int x_center, int y_center, float radius, float limit, Properties &properties)
{
    if (arrow.angle + ARROW_SPEED_BASE <= limit)
    {
        arrow.angle += ARROW_SPEED_BASE;

        ImageResizeAndRotate(arrow.image_base, arrow.image, ARROW_SIZE_W * properties.zoomX, ARROW_SIZE_H * properties.zoomY, -arrow.angle);
        ArrowCalculNewPos(arrow, x_center, y_center, radius, properties);
    }
}

void ArrowDecreaseAngle(Arrow &arrow, int x_center, int y_center, float radius, float limit, Properties &properties)
{
    if (arrow.angle - ARROW_SPEED_BASE >= limit)
    {
        arrow.angle -= ARROW_SPEED_BASE;

        ImageResizeAndRotate(arrow.image_base, arrow.image, ARROW_SIZE_W * properties.zoomX, ARROW_SIZE_H * properties.zoomY, -arrow.angle);
        ArrowCalculNewPos(arrow, x_center, y_center, radius, properties);
    }
}

void ArrowCalculNewPos(Arrow &arrow, int x_center, int y_center, float radius, Properties &properties)
{
    float angle_radian = (arrow.angle * M_PI) / 180;

    float begin_arrowX = x_center + cos(angle_radian) * radius;
    float begin_arrowY = y_center + sin(angle_radian) * radius;

    float center_arrowX = begin_arrowX + cos(angle_radian) * (ARROW_SIZE_W / 2);
    float center_arrowY = begin_arrowY + sin(angle_radian) * (ARROW_SIZE_W / 2);

    float arrow_posX = center_arrowX - (arrow.image.rect_dst.w / (2 * properties.zoomX));
    float arrow_posY = center_arrowY - (arrow.image.rect_dst.h / (2 * properties.zoomY));

    if (arrow_posX - (int) arrow_posX > 0.5)
        arrow.x = arrow_posX + 1;
    else
        arrow.x = arrow_posX;

    if (arrow_posY - (int) arrow_posY > 0.5)
        arrow.y = arrow_posY + 1;
    else
        arrow.y = arrow_posY;

    arrow.image.rect_src = arrow.image.surface->clip_rect;

    arrow.image.rect_dst.x = arrow.x * properties.zoomX;
    arrow.image.rect_dst.y = arrow.y * properties.zoomY;
    arrow.image.rect_dst.w = arrow.image.surface->clip_rect.w;
    arrow.image.rect_dst.h = arrow.image.surface->clip_rect.h;
}
