#include "Input.h"

Input CreateInput()
{
    Input input;
    for (int i = 0;i < SDLK_LAST;i++)
        input.keys[i] = false;

    for (int i = 0;i < 8;i++)
        input.buttons[i] = false;

    input.end = false;
    input.x = 0;
    input.y = 0;
    return input;
}

void InputUpdate(Input &input)
{
    input.resize = false;
    while(SDL_PollEvent(&input.event))
    {
        switch (input.event.type)
        {
            case SDL_QUIT :
                input.end = true;
                break;

            case SDL_KEYDOWN :
                input.keys[input.event.key.keysym.sym] = true;
                break;

            case SDL_KEYUP :
                input.keys[input.event.key.keysym.sym] = false;
                break;

            case SDL_MOUSEBUTTONDOWN :
                input.buttons[input.event.button.button] = true;
                break;

            case SDL_MOUSEBUTTONUP :
                input.buttons[input.event.button.button] = false;
                break;

            case SDL_MOUSEMOTION :
                input.x = input.event.motion.x;
                input.y = input.event.motion.y;
                break;

            case SDL_VIDEORESIZE :
                input.resize = true;
                input.new_width = input.event.resize.w;
                input.new_height = input.event.resize.h;
                break;

            default :
                break;
        }
    }
}
