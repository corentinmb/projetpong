#include "Window.h"

using namespace std;

Window CreateWindow(std::string title, int width, int height)
{
    SDL_Init(SDL_INIT_EVERYTHING);
    TTF_Init();
    SDL_WM_SetCaption(title.c_str(), 0);
    srand(time(NULL));
    char window_pos_centered[] = "SDL_VIDEO_WINDOW_POS=center";
    putenv(window_pos_centered);

    Window window;

    window.screen = SDL_SetVideoMode(width, height, 32, SDL_HWSURFACE | SDL_DOUBLEBUF | SDL_RESIZABLE);
    window.input = CreateInput();
    window.mode = MENU;
    window.framerate.framerate = 60;
    window.framerate.begin = 0;
    window.properties = CreateProperties(width, height);
    window.menu = CreateMenu(window.properties);
    window.game = CreateGame(window.properties);
    WindowRefreshResize(window);

    return window;
}

void DeleteWindow(Window &window)
{
    DeleteMenu(window.menu);
    DeleteGame(window.game);

    SDL_Quit();
}

void WindowUpdateEvent(Window &window)
{
    InputUpdate(window.input);

    if (window.input.resize)
    {
        PropertiesReset(window.properties, window.input.new_width, window.input.new_height);
        WindowRefreshResize(window);
    }

    int elapsed_time = SDL_GetTicks() - window.framerate.begin;
    if (elapsed_time < 1000 / window.framerate.framerate)
        SDL_Delay((1000 / window.framerate.framerate) - elapsed_time);
    window.framerate.begin = SDL_GetTicks();
}

void WindowClear(Window &window)
{
    if (window.screen != 0)
        SDL_FillRect(window.screen, &window.screen->clip_rect, SDL_MapRGB(window.screen->format, 255, 255, 255));
}

void WindowCopy(Window &window, Image& image)
{
    if (window.screen != 0 && image.surface != 0)
        SDL_BlitSurface(image.surface, &image.rect_src, window.screen, &image.rect_dst);
}

void WindowDisplay(Window &window)
{
    if (window.screen != 0)
        SDL_Flip(window.screen);
}

void WindowRefreshResize(Window &window)
{
    if (window.screen != 0)
    {
        SDL_FreeSurface(window.screen);
        window.screen = SDL_SetVideoMode(window.properties.window_width, window.properties.window_height, 32, SDL_HWSURFACE | SDL_DOUBLEBUF | SDL_RESIZABLE);
        MenuResize(window.menu, window.properties);
        GameResize(window.game, window.properties);
    }
}
