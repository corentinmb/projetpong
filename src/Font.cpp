#include "Font.h"

using namespace std;

Image CreateText(string text, string font_file, int font_size)
{
    Image image;

    TTF_Font *font = TTF_OpenFont(font_file.c_str(), font_size);
    if (font != 0)
    {
        SDL_Color text_color = {0, 0, 0};
        image.surface = TTF_RenderText_Blended(font, text.c_str(), text_color);
        if (image.surface != 0)
        {
            image.rect_src = image.surface->clip_rect;
            image.rect_dst = image.surface->clip_rect;
        }
        TTF_CloseFont(font);
    }

    return image;
}

Image CreateText(string text, string font_file, int font_size, Uint8 r, Uint8 g, Uint8 b)
{
    Image image;

    TTF_Font *font = TTF_OpenFont(font_file.c_str(), font_size);
    if (font != 0)
    {
        SDL_Color text_color = {r, g, b};
        image.surface = TTF_RenderText_Blended(font, text.c_str(), text_color);
        if (image.surface != 0)
        {
            image.rect_src = image.surface->clip_rect;
            image.rect_dst = image.surface->clip_rect;
        }
        TTF_CloseFont(font);
    }

    return image;
}

void TextPos(Image &image, int posX, int posY)
{
    image.rect_dst.x = posX;
    image.rect_dst.y = posY;
}
