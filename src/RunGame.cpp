#include "Window.h"
#include "Game.h"

/*  ####################   INITIALISATION DU PONG   ##################### */
Game CreateGame(Properties &properties)
{
    Game game;
    game.back_base = CreateImage("ressources/img/Fond.png");
    ImageOptimize(game.back_base);
    game.back.surface = 0;
    ImageResizeSize(game.back_base, game.back, WINDOW_WIDTH_BASE, WINDOW_HEIGHT_BASE);

    game.ball = CreateBall();
    ImageSetColorKey(game.ball.image, 0, 0, 0);

    game.racket1 = CreateRacket(RACKET1_POS_X, RACKET_POS_Y, SDLK_w, SDLK_s, SDLK_d, SDLK_a, "ressources/img/Racket2.png");
    ImageSetColorKey(game.racket1.image, 0, 0, 0);

    game.racket2 = CreateRacket(RACKET2_POS_X - RACKET_SIZE_W, RACKET_POS_Y, SDLK_UP, SDLK_DOWN, SDLK_RIGHT, SDLK_LEFT, "ressources/img/Racket1.png");
    ImageSetColorKey(game.racket2.image, 0, 0, 0);

    game.score_player1 = CreateScore();
    ScoreSetPos(game.score_player1, 0, 0);

    game.score_player2 = CreateScore();
    ScoreSetPos(game.score_player2, 100, 0);

    game.arrow_racket1 = CreateArrowForPlayer1(game.racket1, properties);
    game.arrow_racket2 = CreateArrowForPlayer2(game.racket2, properties);

    game.message_player1 = CreateMessageWinner(WINNER_PLAYER1);
    game.message_player2 = CreateMessageWinner(WINNER_PLAYER2);
    game.winner = NONE;

    return game;
}
/*  ##################################################################### */


/*  ##################   BOUCLE D'AFFICHAGE DU PONG   ################### */
WindowMode WindowRunGame(Window &window, Game &game)
{
    WindowCopy(window, game.back);

    WindowCopy(window, game.ball.image);

    WindowCopy(window, game.racket1.image);
    WindowCopy(window, game.racket2.image);

    WindowCopy(window, game.score_player1.image);
    WindowCopy(window, game.score_player2.image);

    if (game.arrow_racket1.mode == ARROW_FOCUS_RACKET1)
        WindowCopy(window, game.arrow_racket1.image);

    if (game.arrow_racket2.mode == ARROW_FOCUS_RACKET2)
        WindowCopy(window, game.arrow_racket2.image);

    switch (game.winner)
    {
        case PLAYER1 :
            WindowCopy(window, game.message_player1.message);
            WindowCopy(window, game.message_player1.button);
            GameReset(game, window.properties);
            if (MessageWinnerRun(game.message_player1, window.properties, window.input, window.screen))
            {
                game.winner = NONE;
                return MENU;
            }
            break;

        case PLAYER2 :
            WindowCopy(window, game.message_player2.message);
            WindowCopy(window, game.message_player2.button);
            GameReset(game, window.properties);
            if (MessageWinnerRun(game.message_player2, window.properties, window.input, window.screen))
            {
                game.winner = NONE;
                return MENU;
            }
            break;

        default:

            if (window.input.keys[SDLK_ESCAPE])
            {
                GameReset(game, window.properties);
                return MENU;
            }

            if (game.ball.mode == BALL_FREE)
            {
                game.arrow_racket1.mode = ARROW_FREE;
                game.arrow_racket2.mode = ARROW_FREE;
            }
            else if (game.ball.mode == BALL_FOCUS_RACKET1)
                game.arrow_racket1.mode = ARROW_FOCUS_RACKET1;
            else if (game.ball.mode == BALL_FOCUS_RACKET2)
                game.arrow_racket2.mode = ARROW_FOCUS_RACKET2;

            if (!game.ball.mode)
                BallMovement(game.ball, window.properties);

            BallHeightBorder(game.ball, window.properties);
            BallCollisionRacket(game.ball, game.racket1, game.racket2, window.properties);
            game.winner = BallScore(game.ball, game.racket1, game.racket2, game.arrow_racket1, game.arrow_racket2, game.score_player1, game.score_player2, window.properties);

            GameManageInput(game, window.input, window.properties);
            break;
    }

    return GAME;
}
/*  ##################################################################### */

void GameManageInput(Game &game, Input &input, Properties &properties)
{
    if (input.keys[SDLK_SPACE])
    {
        if (game.ball.mode == BALL_CENTERED)
            game.ball.mode = BALL_FREE;

        if (game.ball.mode == BALL_FOCUS_RACKET1)
        {
            BallTakeArrowAngle(game.ball, game.arrow_racket1);
            game.ball.mode = BALL_FREE;
            ArrowReset(game.arrow_racket1, properties);
            ArrowReset(game.arrow_racket2, properties);
        }
        else if (game.ball.mode == BALL_FOCUS_RACKET2)
        {
            BallTakeArrowAngle(game.ball, game.arrow_racket2);
            game.ball.mode = BALL_FREE;
            ArrowReset(game.arrow_racket1, properties);
            ArrowReset(game.arrow_racket2, properties);
        }
    }

    if (input.keys[game.racket1.key_up])
    {
        RacketMoveUp(game.racket1, properties);
        if (game.ball.mode == BALL_FOCUS_RACKET1)
        {
            BallFocusOnPlayer1(game.ball, game.racket1, properties);
            if (game.arrow_racket1.mode == ARROW_FOCUS_RACKET1);
                ArrowFollowPlayerUp(game.arrow_racket1, game.racket1, properties);
        }
    }
    else if (input.keys[game.racket1.key_down])
    {
        RacketMoveDown(game.racket1, properties);
        if (game.ball.mode == BALL_FOCUS_RACKET1)
        {
            BallFocusOnPlayer1(game.ball, game.racket1, properties);
            if (game.arrow_racket1.mode == ARROW_FOCUS_RACKET1);
                ArrowFollowPlayerDown(game.arrow_racket1, game.racket1, properties);
        }
    }
    else
        game.racket1.mode = RACKET_STATIONNARY;

    if (input.keys[game.racket1.key_left] && game.arrow_racket1.mode == ARROW_FOCUS_RACKET1)
        ArrowDecreaseAngle(game.arrow_racket1, game.ball.x + BALL_SIZE / 2, game.ball.y + BALL_SIZE / 2, BALL_SIZE / 2 + 8, -60, properties);

    if (input.keys[game.racket1.key_right])
    {
        if (game.arrow_racket1.mode == ARROW_FOCUS_RACKET1)
            ArrowIncreaseAngle(game.arrow_racket1, game.ball.x + BALL_SIZE / 2, game.ball.y + BALL_SIZE / 2, BALL_SIZE / 2 + 8, 60, properties);
        else if (game.ball.mode == BALL_FREE && game.racket1.push != RACKET_PUSHED)
            RacketPushPlayer1(game.racket1, properties);
    }
    else
        RacketUnpushPlayer1(game.racket1, properties);




    if (input.keys[game.racket2.key_up])
    {
        RacketMoveUp(game.racket2, properties);
        if (game.ball.mode == BALL_FOCUS_RACKET2)
        {
            BallFocusOnPlayer2(game.ball, game.racket2, properties);
            if (game.arrow_racket2.mode == ARROW_FOCUS_RACKET2);
                ArrowFollowPlayerUp(game.arrow_racket2, game.racket2, properties);
        }
    }
    else if (input.keys[game.racket2.key_down])
    {
        RacketMoveDown(game.racket2, properties);
        if (game.ball.mode == BALL_FOCUS_RACKET2)
        {
            BallFocusOnPlayer2(game.ball, game.racket2, properties);
            if (game.arrow_racket2.mode == ARROW_FOCUS_RACKET2);
                ArrowFollowPlayerDown(game.arrow_racket2, game.racket2, properties);
        }
    }
    else
        game.racket2.mode = RACKET_STATIONNARY;


    if (input.keys[game.racket2.key_left])
    {
        if (game.arrow_racket2.mode == ARROW_FOCUS_RACKET2)
            ArrowDecreaseAngle(game.arrow_racket2, game.ball.x + BALL_SIZE / 2, game.ball.y + BALL_SIZE / 2, BALL_SIZE / 2 + 8, 120, properties);
        else if (game.ball.mode == BALL_FREE && game.racket2.push != RACKET_PUSHED)
            RacketPushPlayer2(game.racket2, properties);
    }
    else
        RacketUnpushPlayer2(game.racket2, properties);

    if (input.keys[game.racket2.key_right] && game.arrow_racket2.mode == ARROW_FOCUS_RACKET2)
        ArrowIncreaseAngle(game.arrow_racket2, game.ball.x + BALL_SIZE / 2, game.ball.y + BALL_SIZE / 2, BALL_SIZE / 2 + 8, 240, properties);

}

/*  #################   REDIMENSIONNEMENT DES IMAGES   ################## */
void GameResize(Game &game, Properties &properties)
{
    ImageResizeSize(game.back_base, game.back, properties.window_width, properties.window_height);

    game.back.rect_dst = game.back.surface->clip_rect;
    game.back.rect_src = game.back.surface->clip_rect;

    BallResize(game.ball, properties);
    ImageSetColorKey(game.ball.image, 0, 0, 0);

    RacketResize(game.racket1, properties);
    ImageSetColorKey(game.racket1.image, 0, 0, 0);

    RacketResize(game.racket2, properties);
    ImageSetColorKey(game.racket2.image, 0, 0, 0);

    ArrowResize(game.arrow_racket1, properties);
    ArrowResize(game.arrow_racket2, properties);

    MessageWinnerResize(game.message_player1, properties);
    MessageWinnerResize(game.message_player2, properties);

    ScoreResizePlayer1(game.score_player1, properties);
    ScoreResizePlayer2(game.score_player2, properties);
}
/*  ##################################################################### */

void GameReset(Game &game, Properties &properties)
{
    BallReset(game.ball, properties);
    RacketReset(game.racket1, properties);
    RacketReset(game.racket2, properties);
    ScoreResetPlayer1(game.score_player1, properties);
    ScoreResetPlayer2(game.score_player2, properties);
    game.arrow_racket1.mode = ARROW_FREE;
    game.arrow_racket2.mode = ARROW_FREE;
}



/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ELEMENTS DE LA STRUCTURE GAME :
- back_base : correspond � l'image de fond � la taille initiale. Permet de fabriquer de nouvelles
        images redimensionn�es de bonne qualit�.

- back : correspond � l'image de fond de la taille de la fen�tre. C'est cette image et SEULEMENT
        cette image qui doit �tre affich�e, et non "back_base".

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ELEMENTS DE "WINDOW" POUVANT SERVIR (UNIQUEMENT DURANT L'AFFICHAGE) :
- window.input.keys[SDLKey] :
        renvoie un bool�en qui indique si la touche entre crochet est appuy�e.

- window.input.button[BoutonSouris] :
        renvoie un bool�en qui indique si le bouton de la souris entre crochet est appuy�.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
FONCTIONS POUVANT SERVIR (UNIQUEMENT DURANT L'AFFICHAGE) :
- WindowCopy(window, image) :
        Copie "image" sur l'�cran.

- ImageResize(image_a_redimensionner, image_redimensionn�, nouvelle_largeur, nouvelle_hauteur) :
        Prend l'image � redimensionner et la redimensionne � la largeur et hauteur donn�e.
        L'image redimensionn�e est ensuite stock�e dans "image_redimensionn�e".
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
COMMENT SE SERVIR DE "PROPERTIES" :

Properties est une structure qui contient les nouvelles informations sur les dimensions
de la fen�tre. Voici ses attributs :

properties.window_width   : largeur de la fen�tre actuelle
properties.window_height  : hauteur de la fen�tre actuelle
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/
