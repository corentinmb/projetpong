#include "Racket.h"
#include "Properties.h"

using namespace std;

Racket CreateRacket(int posX, int posY, SDLKey key_up_chosen, SDLKey key_down_chosen, SDLKey key_right_chosen, SDLKey key_left_chosen, string path)
{
    Racket racket;
    racket.image.surface = 0;

    racket.key_up = key_up_chosen;
    racket.key_down = key_down_chosen;
    racket.key_right = key_right_chosen;
    racket.key_left = key_left_chosen;

    racket.image_base = CreateImage(path);
    ImageResizeSize(racket.image_base, racket.image, RACKET_SIZE_W, RACKET_SIZE_H);

    racket.x = posX;
    racket.y = posY;
    racket.x_base = posX;
    racket.y_base = posY;
    racket.image.rect_dst.x = posX;
    racket.image.rect_dst.y = posY;

    racket.speed = RACKET_SPEED_BASE;
    racket.mode = RACKET_STATIONNARY;
    racket.push = RACKET_UNPUSHED;

    return racket;
}

void DeleteRacket(Racket &racket)
{
    DeleteImage(racket.image_base);
    DeleteImage(racket.image);
}

void RacketMoveUp(Racket &racket, Properties &properties)
{
    if (racket.y - RACKET_SPEED_BASE > BOUND_Y_MIN)
    {
        racket.mode = RACKET_UP;
        racket.y -= RACKET_SPEED_BASE;
    }
    else
        racket.y = BOUND_Y_MIN;

    racket.image_base.rect_dst.y = racket.y;
    racket.image.rect_dst.y = racket.y * properties.zoomY;
}

void RacketMoveDown(Racket &racket, Properties &properties)
{
    if (racket.y + RACKET_SPEED_BASE + RACKET_SIZE_H < BOUND_Y_MAX)
    {
        racket.mode = RACKET_DOWN;
        racket.y += RACKET_SPEED_BASE;
    }

    else
        racket.y = BOUND_Y_MAX - RACKET_SIZE_H;

    racket.image_base.rect_dst.y = racket.y;
    racket.image.rect_dst.y = racket.y * properties.zoomY;
}

void RacketReset(Racket &racket, Properties &properties)
{
    racket.x = racket.x_base;
    racket.y = racket.y_base;

    racket.image.rect_dst.x = racket.x * properties.zoomX;
    racket.image.rect_dst.y = racket.y * properties.zoomY;
    racket.image.rect_dst.w = RACKET_SIZE_W * properties.zoomX;
    racket.image.rect_dst.h = RACKET_SIZE_H * properties.zoomY;

    racket.speed = RACKET_SPEED_BASE;
    racket.mode = RACKET_STATIONNARY;
}

void RacketResize(Racket &racket, Properties &properties)
{
    ImageResizeSize(racket.image_base, racket.image, RACKET_SIZE_W * properties.zoomX, RACKET_SIZE_H * properties.zoomY);

    racket.image.rect_src = racket.image.surface->clip_rect;

    racket.image.rect_dst.x = racket.x * properties.zoomX;
    racket.image.rect_dst.y = racket.y * properties.zoomY;
    racket.image.rect_dst.w = RACKET_SIZE_W * properties.zoomX;
    racket.image.rect_dst.h = RACKET_SIZE_H * properties.zoomY;
}

void RacketPushPlayer1(Racket &racket, Properties &properties)
{
    if (racket.x < racket.x_base + PUSH_DISTANCE)
    {
        racket.x += PUSH_SPEED;
        racket.image.rect_dst.x = racket.x * properties.zoomX;
        racket.push = RACKET_PUSHING;
    }
    else
        racket.push = RACKET_PUSHED;
}

void RacketUnpushPlayer1(Racket &racket, Properties &properties)
{
    racket.push = RACKET_UNPUSHED;
    racket.x = racket.x_base;
    racket.image.rect_dst.x = racket.x * properties.zoomX;
}

void RacketPushPlayer2(Racket &racket, Properties &properties)
{
    if (racket.x > racket.x_base - PUSH_DISTANCE)
    {
        racket.x -= PUSH_SPEED;
        racket.image.rect_dst.x = racket.x * properties.zoomX;
        racket.push = RACKET_PUSHING;
    }
    else
        racket.push = RACKET_PUSHED;
}

void RacketUnpushPlayer2(Racket &racket, Properties &properties)
{
    racket.push = RACKET_UNPUSHED;
    racket.x = racket.x_base;
    racket.image.rect_dst.x = racket.x * properties.zoomX;
}
