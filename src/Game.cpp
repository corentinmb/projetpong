#include "Game.h"

void DeleteGame(Game &game)
{
    DeleteImage(game.back_base);
    DeleteImage(game.back);

    DeleteRacket(game.racket1);
    DeleteRacket(game.racket2);

    DeleteArrow(game.arrow_racket1);
    DeleteArrow(game.arrow_racket2);

    DeleteMessageWinner(game.message_player1);
    DeleteMessageWinner(game.message_player2);
}

// LE FONCTIONNEMENT DE GAME SE TROUVE DANS LE FICHIER "RunGame.cpp".
