#include "Window.h"

int main(int argc, char **argv)
{
    Window window = CreateWindow("Pong");

    while(!window.input.end)
    {
        WindowUpdateEvent(window);

        WindowClear(window);
        switch(window.mode)
        {
            case MENU :
                window.mode = WindowRunMenu(window, window.menu);
                break;
            case GAME :
                window.mode = WindowRunGame(window, window.game);
                break;
            case END :
                window.input.end = true;
                break;
        }
        WindowDisplay(window);
    }

    DeleteWindow(window);

    return 0;
}
