#ifndef WINDOW_H
#define WINDOW_H

#include "Image.h"
#include "Font.h"
#include "Input.h"
#include "Menu.h"
#include "Game.h"
#include "Properties.h"
#include <string>
#include <iostream>

enum WindowMode
{
    MENU,
    GAME,
    END
};

struct Framerate
{
    int framerate;
    int begin;
};

struct Window
{
    SDL_Surface* screen;
    Input input;
    WindowMode mode;
    Framerate framerate;

    Menu menu;
    Game game;

    Properties properties;
};

Window CreateWindow(std::string title, int width = WINDOW_WIDTH_BASE, int height = WINDOW_HEIGHT_BASE);
void DeleteWindow(Window &window);

void WindowUpdateEvent(Window &window);

void WindowClear(Window &window);
void WindowCopy(Window &window, Image &image);
void WindowDisplay(Window &window);

void WindowRefreshResize(Window &window);

WindowMode WindowRunMenu(Window &window, Menu &menu);
WindowMode WindowRunGame(Window &window, Game &game);

#endif // WINDOW_H
