#ifndef GAME_H
#define GAME_H

#include "Image.h"
#include "Input.h"
#include "Properties.h"
#include "Arrow.h"
#include "Racket.h"
#include "Ball.h"
#include "Score.h"
#include "MessageWinner.h"

/* Auteur : Corentin Moreau */

/* STRUCTURE : Game
** D�finit le d�roulement d'une partie de Pong.
*/

struct Game
{
    Image back_base;                    //Image du fond du jeu � une r�solution HD. Permet des redimensionnements de bonne qualit�.
    Image back;                         //Image de fond du jeu destin�e � �tre affich�e.

    Racket racket1;                     //Instance d'une Racket pour le joueur1.
    Arrow arrow_racket1;                //Instance d'une Arrow pour la raquette du joueur1.
    Score score_player1;                //Instance d'un Score pour g�rer le score.
    MessageWinner message_player1;      //Instance d'un MessageWinner pour g�rer la fin d'une partie de Pong et afficher le gagnant.

    Racket racket2;                     //Instance d'une Racket pour le joueur2.
    Arrow arrow_racket2;                //Instance d'une Arrow pour la raquette du joueur2.
    Score score_player2;                //Instance d'un Score pour g�rer le score.
    MessageWinner message_player2;      //Instance d'un MessageWinner pour g�rer la fin d'une partie de Pong et afficher le gagnant.

    Ball ball;                          //Instance d'une Ball pour le Pong.
    Winner winner;                      //Indique s'il y a un gagnant ou non.
};

//Cr��, initialise et retourne une structure Game.
Game CreateGame(Properties &properties);
//Efface de la m�moire toutes les images utilis�es de la structure Game.
void DeleteGame(Game &game);

void GameResize(Game &game, Properties &properties);
void GameReset(Game &game, Properties &properties);
void GameManageInput(Game &game, Input &input, Properties &properties);

#endif // GAME_H
