#ifndef FONT_H_INCLUDED
#define FONT_H_INCLUDED

#include <string>
#include <iostream>
#include <SDL/SDL_ttf.h>

#include "Image.h"

/* AUTEUR : Cl�ment Jacquet */

//Cr��, initialise et retourne l'image d'un texte avec une police et une taille donn�es.
Image CreateText(std::string text, std::string font_file, int font_size);
//Cr��, initialise et retourne l'image d'un texte avec une police, une taille et une couleur donn�es.
Image CreateText(std::string text, std::string font_file, int font_size, Uint8 r, Uint8 g, Uint8 b);

//Change la position d'un texte.
void TextPos(Image &image, int posX, int posY);

#endif // FONT_H_INCLUDED
