#ifndef BALL_H_INCLUDED
#define BALL_H_INCLUDED

#include "Properties.h"
#include "Image.h"
#include "Racket.h"
#include "Arrow.h"
#include "Score.h"

#include <cmath>
#include <iostream>
#include <cstdlib>
#include <ctime>

/* AUTEUR : Dimitri Lallement */

/* ENUMERATION : Ball_Mode
** R�le : D�finit et renseigne sur le comportement de la balle.
*/
enum Ball_Mode
{
    BALL_FREE,              //La balle est libre : elle se d�place.
    BALL_CENTERED,          //La balle est centr�e, immobile.
    BALL_FOCUS_RACKET1,     //La balle suit la raquette du joueur1
    BALL_FOCUS_RACKET2      //La balle suit la raquette du joueur2
};

/*  STRUCTURE : Ball
**  R�le : C'est la structure de la balle de jeu du Pong. Elle d�finit son
**  comportement dans l'environnement du Pong, et subit des facteurs comme les
**  raquettes, les fl�ches, les bordures.
*/

struct Ball
{
    Ball_Mode mode;     //Mode de la balle

    float x;            //Position du coin haut gauche sur l'axe des abscisses.
    float y;            //Position du coin haut gauche sur l'axe des ordonn�es.
    float angle;        //Direction de la balle en radian.

    float speed_base;   //Vitesse de base de la balle. Sert lors de la r�initialisation de la balle.
    float speed;        //Vitesse de la balle.

    Image image_base;   //Image de base de la balle en r�solution HD. Sert de base pour les redimensionnements.
    Image image;        //Image de la balle, destin�e � �tre affich�e.
};

//Cr��, initialise et retourne une structure Ball.
Ball CreateBall();
//Efface de la m�moire les images de la structure Ball.
void DeleteBall(Ball &ball);

//D�finit une nouvelle position pour la balle.
void BallSetPos(Ball &ball, int x, int y, Properties &properties);

//D�finit les bordures de hauteur pour la balle.
void BallHeightBorder(Ball &ball, Properties &properties);
//D�finit le rebond de la balle sur les raquettes.
void BallCollisionRacket(Ball &ball, Racket &racket_player1, Racket &racket_player2, Properties &properties);

//Modifie la position de la balle pour simuler un mouvement.
void BallMovement(Ball &ball, Properties &properties);

//La balle s'initialise sur la raquette du joueur1.
void BallInitPlayer1 (Ball &ball, Racket &racket_player1, Arrow &arrow_player1, Properties &properties);
//La balle s'initialise sur la raquette du joueur2.
void BallInitPlayer2 (Ball &ball, Racket &racket_player2, Arrow &arrow_player2, Properties &properties);

//La balle suit la raquette du joueur1.
void BallFocusOnPlayer1(Ball &ball, Racket &racket_player1, Properties &properties);
//La balle suit la raquette du joueur2.
void BallFocusOnPlayer2(Ball &ball, Racket &racket_player2, Properties &properties);

//Fonction complexe qui sert � d�terminer si la balle � d�passer les limites de largeur, � augmenter le score, � r�initialiser la raquette qui a perdu et associe une fl�che � la raquette perdante.
Winner BallScore(Ball &ball, Racket &racket_player1, Racket &racket_player2, Arrow &arrow_player1, Arrow &arrow_player2, Score &score_player1, Score &score_player2, Properties &properties);

//Permet de r�initialiser la balle.
void BallReset(Ball &ball, Properties &properties);
//Permet de redimensionner la balle.
void BallResize(Ball &ball, Properties &properties);

//La balle prend l'angle d'une fl�che.
void BallTakeArrowAngle(Ball &ball, Arrow &arrow);

//Retourne un angle al�atoire suivant deux bornes, une maximale et une minimale.
float GetRandomAngle(float bound1, float bound2);
//Retourne un angle al�atoire compris entre deux intervalles d�finies par les bornes donn�es en param�tres.
float GetRandomAngle(float bound1, float bound2, float bound3, float bound4);


#endif // BALL_H_INCLUDED
