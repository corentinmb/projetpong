#ifndef MENU_H
#define MENU_H

#include "Image.h"
#include "Input.h"
#include "Properties.h"


struct Menu
{
    Image back_base;
    Image title_base;
    Image button_base;
    Image back;
    Image title;
    Image button;
    SDL_Rect rect_src[4];
    SDL_Rect rect_dst[4];

    int posX_title;
    int posY_title;

    int posX_button;
    int posY_play;
    int posY_quit;
};

Menu CreateMenu(Properties properties);
void DeleteMenu(Menu &menu);

void MenuResize(Menu &menu, Properties properties);
void MenuInitRect(SDL_Rect &rect, int x, int y, int w, int h);

#endif // MENU_H
