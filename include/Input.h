#ifndef INPUT_H_INCLUDED
#define INPUT_H_INCLUDED

#include <SDL/SDL.h>

struct Input
{
    SDL_Event event;
    bool keys[SDLK_LAST];
    bool buttons[8];
    bool end;

    int x;
    int y;

    bool resize;
    int new_width;
    int new_height;
};

Input CreateInput();
void InputUpdate(Input &input);

#endif // INPUT_H_INCLUDED
