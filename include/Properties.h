#ifndef PROPERTIES_H
#define PROPERTIES_H

/*
    Toutes les propri�t�s du programme.
    Elles sont toutes modifiables, selon les pr�f�rences de l'utilisateur.
    Attention n�anmoins � ne pas trop modifier sans quoi le Pong risque
    d'�tre d�faillant.
*/

#define WINDOW_WIDTH_BASE 800
#define WINDOW_HEIGHT_BASE 500

#define BUTTON_FILE_WIDTH 425
#define BUTTON_FILE_HEIGHT 175

#define BUTTON_WIDTH 200
#define BUTTON_HEIGHT 75

#define BUTTON_SRC_X 225
#define BUTTON_SRC_Y 100

#define TITLE_FILE_WIDTH 860
#define TITLE_FILE_HEIGHT 240

#define BALL_SIZE 8

#define RACKET_SIZE_W 8
#define RACKET_SIZE_H 50

#define RACKET1_POS_X 25
#define RACKET2_POS_X 775
#define RACKET_POS_Y 229

#define BOUND_Y_MIN 27
#define BOUND_Y_MAX 473
#define BOUND_X_MIN 25
#define BOUND_X_MAX 775

#define MESSAGE_WINNER_POS_X 200
#define MESSAGE_WINNER_POS_Y 75
#define MESSAGE_WINNER_SIZE_X 400
#define MESSAGE_WINNER_SIZE_Y 250

#define BUTTON_BACK_POS_X 275
#define BUTTON_BACK_POS_Y 200
#define BUTTON_BACK_SIZE_X 250
#define BUTTON_BACK_SIZE_Y 80

#define ARROW_SIZE_W 50
#define ARROW_SIZE_H 8

#define SCORE_SIZE 40
#define SCORE_POS_Y 50
#define SCORE_POS_X1 355
#define SCORE_POS_X2 405
#define SCORE_MAX 10

#define ARROW_SPEED_BASE 5
#define BALL_SPEED_BASE 8
#define RACKET_SPEED_BASE 7

#define RACKET_POWER M_PI/12

#define PUSH_DISTANCE 24
#define PUSH_SPEED 4

/* AUTEUR : Cl�ment Jacquet */

/* STRUCTURE : Properties
** R�le : Cette structure contient la taille de la fen�tre et le zoom appliqu� sur la fen�tre
** (par rapport � la taille initiale). Elle est �norm�ment utilis�e (dans � peu pr�s 3/4 des
** fonctions du Pong) car c'est la seule entit� permettant de redimensionner correctements
** toutes les images, les zones d'affichage etc.
*/
struct Properties
{
    int window_width;
    int window_height;

    float zoomX;
    float zoomY;
};

// Cr��, initialise et retourne une structure Properties.
Properties CreateProperties(int width, int height);

// Fonction qui permet de remettre une structure Properties � jour.
void PropertiesReset(Properties &properties, int width, int height);

#endif // PROPERTIES_H
