#ifndef MESSAGEWINNER_H
#define MESSAGEWINNER_H

#include "Image.h"
#include "Properties.h"
#include "Input.h"

enum PlayerWinner
{
    WINNER_PLAYER1,
    WINNER_PLAYER2
};

struct MessageWinner
{
    Image message_base;
    Image message;

    Image button_base;
    Image button;

    Image button_selected_base;
    Image button_selected;
};

MessageWinner CreateMessageWinner(PlayerWinner winner);
void DeleteMessageWinner(MessageWinner &message);

bool MessageWinnerRun(MessageWinner &message, Properties &properties, Input &input, SDL_Surface* screen);
void MessageWinnerResize(MessageWinner &message, Properties &properties);

#endif // MESSAGEWINNER_H
