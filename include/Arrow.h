#ifndef ARROW_H
#define ARROW_H

#include "Image.h"
#include "Racket.h"
#include "Properties.h"

/* AUTEUR : Cl�ment Jacquet */


/* ENUMERATION : ArrowMode
** R�le : Chacun de ses attributs renseigne sur le comportement d'une fl�che.
*/
enum ArrowMode
{
    ARROW_FREE,             //La fl�che est lib�r�e : Elle devient invisible et ne peut pas �tre manipul�e avec les touches
    ARROW_FOCUS_RACKET1,    //La fl�che suit la raquette du joueur1
    ARROW_FOCUS_RACKET2     //La fl�che suit la raquette du joueur2
};


/* STRUCTURE : Arrow
** R�le : Choisir et repr�senter l'angle de la direction de la balle lorsqu'elle est coll�e � l'une des raquettes.
*/

struct Arrow
{
    Image image_base;       //Image de la fl�che en r�solution HD. Sert de base � des redimensionnements de bonne qualit�.
    Image image;            //Image de la fl�che qui sert � �tre affich�e.

    float angle_base;       //Angle de base de la fl�che. Sert � r�initialiser une fl�che.
    float angle;            //Angle d'une fl�che.

    int x_base;             //Position de base sur l'axe des abscisses de la fl�che. Sert � r�initialiser une fl�che.
    int x;                  //Position sur l'axe des abscisses de la fl�che.

    int y_base;             //Position de base sur l'axe des ordonn�es de la fl�che. Sert � r�initialiser une fl�che.
    int y;                  //Position sur l'axe des ordonn�es de la fl�che.

    ArrowMode mode;         //Comportement de la fl�che.
};

//Cr�e, initialise et retourne une structure Arrow en fonction des param�tres donn�es.
Arrow CreateArrow(int x, int y, float angle, Properties &properties);
//Cr�e, initialise et retourne une structure Arrow pour la raquette du joueur 1.
Arrow CreateArrowForPlayer1(Racket &racket, Properties &properties);
//Cr�e, initialise et retourne une structure Arrow pour la raquette du joueur 2.
Arrow CreateArrowForPlayer2(Racket &racket, Properties &properties);

//Efface de la m�moire les images de la structure Arrow
void DeleteArrow(Arrow &arrow);
//Redimensionne une fl�che en fonction des nouvelles dimensions de la fen�tre
void ArrowResize(Arrow &arrow, Properties &properties);

//R�initialise une structure Arrow � son �tat d'origine.
void ArrowReset(Arrow &arrow, Properties &properties);

//Fais appartenir une fl�che au joueur1. Il peut interagir avec.
void ArrowFocusOnPlayer1(Arrow &arrow, Racket &racket_player1, Properties &properties);
//Fais appartenir une fl�che au joueur2. Il peut interagir avec.
void ArrowFocusOnPlayer2(Arrow &arrow, Racket &racket_player2, Properties &properties);

//Fais suivre une fl�che � la raquette du joueur1.
void ArrowFollowPlayerUp(Arrow &arrow, Racket &racket_player, Properties &properties);
//Fais suivre une fl�che � la raquette du joueur2.
void ArrowFollowPlayerDown(Arrow &arrow, Racket &racket_player, Properties &properties);

//Augmente l'angle d'une fl�che en fonction d'un centre de rotation, d'un rayon d'�cart avec ce centre et d'un angle maximal.
void ArrowIncreaseAngle(Arrow &arrow, int x_center, int y_center, float radius, float limit, Properties &properties);
//Diminue l'angle d'une fl�che en fonction d'un centre de rotation, d'un rayon d'�cart avec ce centre et d'un angle minimal.
void ArrowDecreaseAngle(Arrow &arrow, int x_center, int y_center, float radius, float limit, Properties &properties);
//Fonction qui sert � calculer la position de l'image de la fl�che.
void ArrowCalculNewPos(Arrow &arrow, int x_center, int y_center, float radius, Properties &properties);

#endif // ARROW_H
