#ifndef IMAGE_H
#define IMAGE_H

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_rotozoom.h>
#include <string>

struct Image
{
    SDL_Surface* surface;
    SDL_Rect rect_src;
    SDL_Rect rect_dst;
};

Image CreateImage(std::string path);
void DeleteImage(Image &image);

void ImageSetColorKey(Image &image, int r, int g, int b);
void ImageResizeSize(Image &image_src, Image &image_dst, int w, int h);
void ImageResize(Image &image_src, Image &image_dst, float zoomX, float zoomY);
void ImageRotate(Image &image_src, Image &image_dst, float angle);
void ImageResizeAndRotate(Image &image_src, Image &image_dst, int w, int h, float angle);
void ImageOptimize(Image &image);

#endif // IMAGE_H
