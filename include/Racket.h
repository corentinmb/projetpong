#ifndef RACKET_H
#define RACKET_H

#include "Image.h"
#include "Properties.h"


enum RacketModeMovement
{
    RACKET_STATIONNARY,
    RACKET_DOWN,
    RACKET_UP
};

enum RacketModePush
{
    RACKET_PUSHED,
    RACKET_PUSHING,
    RACKET_UNPUSHED
};

struct Racket
{
    Image image_base;
    Image image;

    int x;
    int y;
    int x_base;
    int y_base;

    SDLKey key_up;
    SDLKey key_down;
    SDLKey key_right;
    SDLKey key_left;

    RacketModeMovement mode;
    RacketModePush push;

    int speed;
};

Racket CreateRacket(int posX, int posY, SDLKey key_up_chosen, SDLKey key_down_chosen, SDLKey key_right_chosen, SDLKey key_left_chosen, std::string path);
void DeleteRacket(Racket &racket);

void RacketMoveUp(Racket &racket, Properties &properties);
void RacketMoveDown(Racket &racket, Properties &properties);

void RacketReset(Racket &racket, Properties &properties);
void RacketResize(Racket &racket, Properties &properties);

void RacketPushPlayer1(Racket &racket, Properties &properties);
void RacketUnpushPlayer1(Racket &racket, Properties &properties);

void RacketPushPlayer2(Racket &racket, Properties &properties);
void RacketUnpushPlayer2(Racket &racket, Properties &properties);

#endif // RACKET_H
