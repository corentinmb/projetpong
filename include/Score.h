#ifndef SCORE_H
#define SCORE_H

#include "Font.h"
#include "Properties.h"
#include <sstream>
#include <string>
#include <cstdlib>
#include <cmath>

struct Score
{
    Image image_base;
    Image image;
    int score;
};

enum Winner
{
    NONE,
    PLAYER1,
    PLAYER2
};


Score CreateScore();

void DeleteScore(Score &score);

void ScoreResizePlayer1(Score &score, Properties &properties);
void ScoreResizePlayer2(Score &score, Properties &properties);
void ScoreSetPos(Score &score, int x, int y);

void ScoreIncrementPlayer1(Score &score, Properties &properties);
void ScoreIncrementPlayer2(Score &score, Properties &properties);

void ScoreResetPlayer1(Score &score, Properties &properties);
void ScoreResetPlayer2(Score &score, Properties &properties);


#endif // SCORE_H
